<?php
require_once ('connect.php');

$query  = "SELECT * FROM results ORDER BY id DESC LIMIT 5";
$users = mysqli_query($conn, $query);

$sqlSupportCauses = "SELECT COUNT(name), support_cause FROM results GROUP BY support_cause";
$result = mysqli_query($conn, $sqlSupportCauses);
$votes = mysqli_fetch_all($result, MYSQLI_ASSOC);

$causesVotes = [];

for ($i=1; $i <= 17; $i++) {
    $causesVotes[$i] = 0;
}

foreach ($votes as $vote) {
    $causesVotes[$vote['support_cause']] = $vote['COUNT(name)'];
}

?>
<!DOCTYPE html>
<html lang="bg">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Големите 17</title>
    <link rel="stylesheet" type="text/css" href="dist/style.css" />
    <link rel="stylesheet" type="text/css" href="bootstrap.css">
    <link rel="stylesheet" href="fontawesome-free-5.14.0-web/css/all.css">
</head>
<body>
<header>
    <div class="header">
        <img src="images/header.png" class="img-fluid" alt="">
    </div>
    <?php if(isset($_GET['success'])){ ?>
        <div style="background-color:lightslategrey">
            <p style="font-size:40px;color:white;text-align:center">Успешно приключихте теста</p>
        </div>
    <?php } ?>
</header>
<main class="container">
    <h1 class="fs50 light text-center py-5">Малките стъпки, предприети от много хора, правят чудеса. Ние също помагаме те да се случат. Виж как.</h1>
    <div>
        <h3 class="fs50">ГОЛЕМИТЕ 17</h3>
        <p>са целите за устойчиво развитие, които ООН формулира през 2015 г. Те обединяват хора и организации по пътя към една по-добра реалност. Ние от БНП Париба активно подкрепяме и работим за тези цели вече 5 години - по света и у нас.
        </p>
        <div class="row">
            <div class="col-md-8 position-relative">
                <img id="image1" src="images/start-quiz.gif" alt="">
                <a href="quiz.php" id="quiz-start-btn" class="start-btn">СТАРТ</a>
            </div>
            <div class="col-md-4">
                <p class="clr-blue fs22 n-bold">Запознай се с ГОЛЕМИТЕ 17, докато играеш:</p>
                <ul class="the-great fs22 clr-blue">
                    <li><span>1.</span> Реши пъзелите и събирай точки.</li>
                    <li><span>2.</span> Избери каузата, която най-силно подкрепяш.</li>
                    <li><span>3.</span> Сподели идея как ние като компания можем да я подкрепим.</li>
                </ul>
                <?php if($users) { ?>
                    <h3 class="bold fs50">Последно играха</h3>
                    <div>
                        <ul class="p-3 fs22" style="height: 170px;">
                            <?php foreach($users as $key=>$value) { ?>
                                <li class="row justify-content-between">
                                    <span class="col-md-8 col-8 col-sm-8 p-0"><?php echo $value['name'] ?> </span>
                                    <span class='font-weight-bold'><?php echo $value['score']. ' точки' ?></span>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } ?>
            </div>
        </div>
        <!--<div class="row mt-5">
            <div class="col-md-6">
                <h2 class="fs50 bold"><span><img src="images/mic.png" class="pr-3" alt=""></span>Podcast station</h2>
                <p class="cond"><b class="bold">Очаквай на 21.07:</b> <a href="javascript:;">Podcast  Tuesday - интервю с Елеонора Маринова</a></p>
                <p class="cond"><b class="bold">Очаквай на 23.07:</b> <a href="javascript:;">Podcast  Thursday - интервю с Иван Пармаков</a></p>
            </div>
            <div class="col-md-6">
                <h2 class="fs50 bold"><span><img src="images/hear.png" class="pr-3" alt=""></span>Past Podcasts</h2>
                <div>
                    <p class="cond"><a href="javascript:;"><i class="fas fa-volume-up text-dark"></i>Слушай Podcast Tuesday 14.07 - интервю с Явор Билков</a> </p>
                    <p class="cond"><a href="javascript:;"><i class="fas fa-volume-up"></i>Слушай Podcast Thursday 16.07 - интервю с Петър Кирков</a> </p>
                </div>
            </div>
        </div>-->
        <div class="mt-5">
            <h3 class="fs50 bold">Подкрепени каузи</h3>
            <div class="causes my-5">
                <div>
                    <img src="images/q1.png" class="mr-2" alt="">
                    <div class="count-square">
                        <?php for($i = 0; $i < $causesVotes[1]; $i++ ) { ?>
                            <?php echo '<span></span>' ?>
                        <?php } ?>
                    </div>
                </div>
                <div>
                    <img src="images/q2.png" class="mr-2" alt="">
                    <div class="count-square sq2">
                        <?php for($i = 0; $i < $causesVotes[2]; $i++ ) { ?>
                            <?php echo '<span></span>' ?>
                        <?php } ?>
                    </div>
                </div>
                <div>
                    <img src="images/q3.png" class="mr-2" alt="">
                    <div class="count-square sq3">
                        <?php for($i = 0; $i < $causesVotes[3]; $i++ ) { ?>
                            <?php echo '<span></span>' ?>
                        <?php } ?>
                    </div>
                </div>
                <div>
                    <img src="images/q4.png" class="mr-2" alt="">
                    <div class="count-square sq4">
                        <?php for($i = 0; $i < $causesVotes[4]; $i++ ) { ?>
                            <?php echo '<span></span>' ?>
                        <?php } ?>
                    </div>
                </div>
                <div>
                    <img src="images/q5.png" class="mr-2" alt="">
                    <div class="count-square sq5">
                        <?php for($i = 0; $i < $causesVotes[5]; $i++ ) { ?>
                            <?php echo '<span></span>' ?>
                        <?php } ?>
                    </div>
                </div>
                <div>
                    <img src="images/q6.png" class="mr-2" alt="">
                    <div class="count-square sq6">
                        <?php for($i = 0; $i < $causesVotes[6]; $i++ ) { ?>
                            <?php echo '<span></span>' ?>
                        <?php } ?>
                    </div>
                </div>
                <div>
                    <img src="images/q7.png" class="mr-2" alt="">
                    <div class="count-square sq7">
                        <?php for($i = 0; $i < $causesVotes[7]; $i++ ) { ?>
                            <?php echo '<span></span>' ?>
                        <?php } ?>
                    </div>
                </div>
                <div>
                    <img src="images/q8.png" class="mr-2" alt="">
                    <div class="count-square sq8">
                        <?php for($i = 0; $i < $causesVotes[8]; $i++ ) { ?>
                            <?php echo '<span></span>' ?>
                        <?php } ?>
                    </div>
                </div>
                <div>
                    <img src="images/q9.png" class="mr-2" alt="">
                    <div class="count-square sq9">
                        <?php for($i = 0; $i < $causesVotes[9]; $i++ ) { ?>
                            <?php echo '<span></span>' ?>
                        <?php } ?>
                    </div>
                </div>
                <div>
                    <img src="images/q10.png" class="mr-2" alt="">
                    <div class="count-square sq10">
                        <?php for($i = 0; $i < $causesVotes[10]; $i++ ) { ?>
                            <?php echo '<span></span>' ?>
                        <?php } ?>
                    </div>
                </div>
                <div>
                    <img src="images/q11.png" class="mr-2" alt="">
                    <div class="count-square sq11">
                        <?php for($i = 0; $i < $causesVotes[11]; $i++ ) { ?>
                            <?php echo '<span></span>' ?>
                        <?php } ?>
                    </div>
                </div>
                <div>
                    <img src="images/q12.png" class="mr-2" alt="">
                    <div class="count-square sq12">
                        <?php for($i = 0; $i < $causesVotes[12]; $i++ ) { ?>
                            <?php echo '<span></span>' ?>
                        <?php } ?>
                    </div>
                </div>
                <div>
                    <img src="images/q13.png" class="mr-2" alt="">
                    <div class="count-square sq13">
                        <?php for($i = 0; $i < $causesVotes[13]; $i++ ) { ?>
                            <?php echo '<span></span>' ?>
                        <?php } ?>
                    </div>
                </div>
                <div>
                    <img src="images/q14.png" class="mr-2" alt="">
                    <div class="count-square sq14">
                        <?php for($i = 0; $i < $causesVotes[14]; $i++ ) { ?>
                            <?php echo '<span></span>' ?>
                        <?php } ?>
                    </div>
                </div>
                <div>
                    <img src="images/q15.png" class="mr-2" alt="">
                    <div class="count-square sq15">
                        <?php for($i = 0; $i < $causesVotes[15]; $i++ ) { ?>
                            <?php echo '<span></span>' ?>
                        <?php } ?>
                    </div>
                </div>
                <div>
                    <img src="images/q16.png" class="mr-2" alt="">
                    <div class="count-square sq16">
                        <?php for($i = 0; $i < $causesVotes[16]; $i++ ) { ?>
                            <?php echo '<span></span>' ?>
                        <?php } ?>
                    </div>
                </div>
                <div>
                    <img src="images/q17.png" class="mr-2" alt="">
                    <div class="count-square sq17">
                        <?php for($i = 0; $i < $causesVotes[17]; $i++ ) { ?>
                            <?php echo '<span></span>' ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<footer>
    <div class="pull-left">
        <a class="footer-tos fs22 cond" href="https://www.bnpparibas-pf.bg/media/download-documents/2020/General%20conditions_BNPPPF_the_big_17.pdf" target="blank_" title="Общи условия за ползване">Общи условия за ползване</a>
    </div>
</footer>
</body>
<style>
    @font-face {
        font-family: 'bnpp-sans';
        src: url("font/bnpp-sans.ttf") format("truetype");
    }
    @font-face {
        font-family: 'bnpp-sans-bold';
        src: url("font/bnpp-sans-bold.ttf") format("truetype");
    }
    @font-face {
        font-family: 'bnpp-sans-cond-bold-v2';
        src: url("font/bnpp-sans-cond-bold-v2.ttf") format("truetype");
    }
    @font-face {
        font-family: 'bnpp-sans-cond-extrabold-v2';
        src: url("font/bnpp-sans-cond-extrabold-v2.ttf") format("truetype");
    }
    @font-face {
        font-family: 'bnpp-sans-cond-light-v2';
        src: url("font/bnpp-sans-cond-light-v2.ttf") format("truetype");
    }
    @font-face {
        font-family: 'bnpp-sans-cond-v2';
        src: url("font/bnpp-sans-cond-v2.ttf") format("truetype");
    }
    @font-face {
        font-family: 'bnpp-sans-extrabold';
        src: url("font/bnpp-sans-extrabold.ttf") format("truetype");
    }
    @font-face {
        font-family: 'bnpp-sans-light';
        src: url("font/bnpp-sans-light.ttf") format("truetype");
    }
</style>
</html>