<!DOCTYPE html>
<html lang="bg">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
  <title>Големите 17</title>
  <link rel="stylesheet" type="text/css" href="dist/jquery.quiz-min.css" />
  <link rel="stylesheet" type="text/css" href="dist/style.css"/>
  <link rel="stylesheet" type="text/css" href="bootstrap.css">
  <link rel="stylesheet" href="fontawesome-free-5.14.0-web/css/all.css">
</head>
<body>
<header>
  <div class="header">
    <img src="images/header.png" class="img-fluid" alt="">
  </div>
</header>

<section>
  <div id="quiz">
    <div id="quiz-start-screen">
      <img src="images/start-quiz.gif" class="hidden-mobile" alt="">
      <img src="images/quiz-mobile.png" alt="houdini" class="visible-mobile">
      <p><a href="javascript:;" id="quiz-start-btn" class="btnn">ИГРАЙ</a></p>
    </div>
  </div>
</section>
<footer>
    <div class="pull-left">
        <a class="footer-tos fs22 cond" href="https://www.bnpparibas-pf.bg/media/download-documents/2020/General%20conditions_BNPPPF_the_big_17.pdf" target="blank_" title="Общи условия за ползване">Общи условия за ползване</a>
    </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script src="dist/jquery.quiz-min.js"></script>
<script src="app.js"></script>

<script>
  // Variable to keep track of the total score.
  var totalScore = 0;

  // Provide an answer callback to add points to the score if correct answer selected.
  var answerCallback = function(currentQuestion, selected, question) {
    console.log('ul.answers:nth-child('+ currentQuestion +') li:nth-child('+ (question.correctIndex+1) +') a ')
    // Check if the user got the answer correct.
    if (selected === question.correctIndex) {
      // Add the points as specified in the question parameters, or default to 1.
      totalScore += question.points || 1;
    } 
    // console.log(question.correctIndex+1)

    $('.total-score').html('<span class="py-2 light">Резултат <span class="bold">' + totalScore + ' точки!</span></span>');
    $('.score').html('<span class="py-2 light">Твоят резултат е <span class="bold">' + totalScore + ' точки!</span></span>');

  };

  // Provide a callback to reset the score. Used if the home button is clicked.
  var resetScore = function() {
    totalScore = 0;
  };
  // Provide a finish callback to display the score.
  var finishCallback = function() {
    $('#quiz-results').html('<div> <p class="final-score cond light"> Честито твоят резултат е <b class="bold">' + totalScore + ' точки!</b> </p> <p class="light mt fs25"> Попълни формата, за да се включиш за награда - електрически скутер. </p> <form id="form" action="controller.php" method="POST">  <input type="hidden" name="score" value="'+ totalScore +'"> <div class="row justify-content-center"><div class="col-md-3 p-0 col-sm-12"><label for="" class="cond">Аз съм</label> <input class="w-input ml-3" onkeyup="success()" id="textsend" type="text" name="name" / required></div> <div class="col-md-4 col-sm-12 "><label for="" class="cond">Работя като</label> <input onkeyup="success()" id="profession" class="w-input" type="text" name="profession" / required></div> </div> <div class="row mb-4"> <span class="col-md-3 text-right p-0 cond">Смятам, че моята компанията трябва да подкрепи кауза:</span> <div class="col-md-7 pr-0 c-img image-radio"> <input type="radio" name="supportCause" id="supportCause1" value="1"> <label class="support-cause" for="supportCause1"><img src="images/1.png" alt="" /></label> <input type="radio" name="supportCause" id="supportCause2" value="2"> <label class="support-cause" for="supportCause2"><img src="images/2.png" alt="" /></label> <input type="radio" name="supportCause" id="supportCause3" value="3"> <label class="support-cause" for="supportCause3"><img src="images/3.png" alt="" /></label> <input type="radio" name="supportCause" id="supportCause4" value="4"> <label class="support-cause" for="supportCause4"><img src="images/4.png" alt="" /></label> <input type="radio" name="supportCause" id="supportCause5" value="5"> <label class="support-cause" for="supportCause5"><img src="images/5.png" alt="" /></label> <input type="radio" name="supportCause" id="supportCause6" value="6"> <label class="support-cause" for="supportCause6"><img src="images/6.png" alt="" /></label> <input type="radio" name="supportCause" id="supportCause7" value="7"> <label class="support-cause" for="supportCause7"><img src="images/7.png" alt="" /></label> <input type="radio" name="supportCause" id="supportCause8" value="8"> <label class="support-cause" for="supportCause8"><img src="images/8.png" alt="" /></label> <input type="radio" name="supportCause" id="supportCause9" value="9"> <label class="support-cause" for="supportCause9"><img src="images/9.png" alt="" /></label> <input type="radio" name="supportCause" id="supportCause10" value="10"> <label class="support-cause" for="supportCause10"><img src="images/10.png" alt="" /></label> <input type="radio" name="supportCause" id="supportCause11" value="11"> <label class="support-cause" for="supportCause11"><img src="images/11.png" alt="" /></label> <input type="radio" name="supportCause" id="supportCause12" value="12"> <label class="support-cause" for="supportCause12"><img src="images/12.png" alt="" /></label> <input type="radio" name="supportCause" id="supportCause13" value="13"> <label class="support-cause" for="supportCause13"><img src="images/13.png" alt="" /></label> <input type="radio" name="supportCause" id="supportCause14" value="14"> <label class="support-cause" for="supportCause14"><img src="images/14.png" alt="" /></label> <input type="radio" name="supportCause" id="supportCause15" value="15"> <label class="support-cause" for="supportCause15"><img src="images/15.png" alt="" /></label> <input type="radio" name="supportCause" id="supportCause16" value="16"> <label class="support-cause" for="supportCause16"><img src="images/16.png" alt="" /></label> <input type="radio" name="supportCause" id="supportCause17" value="17"> <label class="support-cause" for="supportCause17"><img src="images/17.png" alt="" /></label> </div> </div> <div class="row"><label for="" class="cond mr-4 col-md-3 text-right pl">Моята свежа идея</label> <input class="textarea" name="idea" type="text"> <div class="d-flex justify-content-center mt-4"></div> <div class="sent-btn"><button type="submit" class="bold" id="button" disabled="disabled">ИЗПРАТИ</button></div> </div> </form> </div>');
    totalScore = 0;
  };

  var questionCount = 0;

  var colors = ["#DDA63A", "#DDA63A","#4C9F38","#C5192D", "#FF3A21", "#26BDE2", "#FCC30B", "#A21942", "#FD6925", "#DD1367", "#FD9D24", "#BF8B2E", "#3F7E44", "#0A97D9", "#56C02B", "#00689D", "#19486A"];
  var colorss = ["#DDA63A","#4C9F38","#C5192D", "#FF3A21", "#26BDE2", "#FCC30B", "#A21942", "#FD6925", "#DD1367", "#FD9D24", "#BF8B2E", "#3F7E44", "#0A97D9", "#56C02B", "#00689D", "#19486A"];

  var changeColor = function() {
    questionCount++
    $("#quiz").css("background-color", colors[questionCount]);
    $("#quiz-controls").css("background-color", colorss[questionCount]);

  }
 
  $('#quiz').quiz({
    // resultsScreen: '#results-screen',
    // counter: false,
    // homeButton: '#custom-home',
    counterFormat: '<span>%current</span> <p class="fs30 light">Довърши символа</p>',
    answerCallback: answerCallback,
    nextButtonText: '<i class="far fa-arrow-alt-circle-right"></i>',
    // Specify finish callback.
    finishCallback: finishCallback,
    finishButtonText: 'Продължи<i class="fas fa-arrow-right"></i>',
    // Specify callback to reset score.
    homeCallback: resetScore,
    nextCallback: changeColor,
    questions: [
      {
        'q': '<span class="ask fs50">Изкореняване на бедността</span><img class="p-5 q-img" src="images/question1.png"><p class="py-2 total-score">Резултат: <span class="bold">0 точки</span></p><div class="first-info"><span class="count l5">1</span><h1>Изкореняване на бедността</h1><p class="fs30">Почти половината от световното население живее в бедност, а липсата на храна и чиста вода убива хиляди всеки ден.</p><p class="fs30">Изкореняването на този проблем не е просто благотворителна кауза, а наш морален дълг и акт на справедливост. Заедно можем да нахраним гладните, да победим болестите и да дадем на всеки човек шанс за по-добър живот.</p><a href="javascript:;"><i class="far fa-arrow-alt-circle-right first"></i></a></div>',
        'options': [
          '<img class="a1" src="images/answer101.png">',
          '<img class="a1" src="images/answer102.png">',
          '<img class="a1" src="images/answer103.png">',
        ],
        'correctIndex': 1,
        'points': 10,
        'correctResponse': '<span class="count l5">2</span><h1>Край на глада</h1><p class="fs30">Гладът е водеща причина за смъртността в цял свят. Въпреки че планетата ни предлага богати ресурси, много хора просто нямат достъп до тях.</p><p class="fs30">Това може да се промени, ако подпомагаме устойчивото земеделие със съвременни технологии и справедливи системи за дистрибуция. Така, един ден ще бъдем сигурни, че вече няма човешко същество, застрашено от глад или недохранване.</p>',
        'incorrectResponse': '<span class="count l5">2</span><h1>Край на глада</h1><p class="fs30">Гладът е водеща причина за смъртността в цял свят. Въпреки че планетата ни предлага богати ресурси, много хора просто нямат достъп до тях.</p><p class="fs30">Това може да се промени, ако подпомагаме устойчивото земеделие със съвременни технологии и справедливи системи за дистрибуция. Така, един ден ще бъдем сигурни, че вече няма човешко същество, застрашено от глад или недохранване.</p>',
      },
      {
        'q': '<span class="ask fs50">Край на глада</span><img class="p-5 q2-img" src="images/hot.png"><p class="py-2 total-score">Резултат: 0 точки</p>',
        'options': [
          '<img class="a2" src="images/poo.png">',
          '<img class="a2" src="images/fish.png">',
          '<img class="a2" class="h50" src="images/bowl.png">',
        ],
        'correctIndex': 2,
        'points': 10,
        'correctResponse': '<span class="count l5">3</span><h1>Добра грижа за здравето</h1><p class="fs30">Въпреки напредъка на медицината и успешната борба с голям брой заболявания, близо 400 милиона хора по света не могат да разчитат дори на елементарна здравна грижа.</p><p class="fs30">Насърчаването на здравословния начин на живот и превенцията е от съществено значение за устойчивото развитие. Като финансираме ефективно здравните системи, подобрим санитарните условия и осигурим лесен достъп до лекари, можем да спасим живота на милиони.</p>',
        'incorrectResponse': '<span class="count l5">3</span><h1>Добра грижа за здравето</h1><p class="fs30">Въпреки напредъка на медицината и успешната борба с голям брой заболявания, близо 400 милиона хора по света не могат да разчитат дори на елементарна здравна грижа.</p><p class="fs30">Насърчаването на здравословния начин на живот и превенцията е от съществено значение за устойчивото развитие. Като финансираме ефективно здравните системи, подобрим санитарните условия и осигурим лесен достъп до лекари, можем да спасим живота на милиони.</p>'
      },
      {
        'q': '<span class="ask fs50">Добра грижа за здравето</span><img class="p-5 q3-img" src="images/hearth.png"><p class="py-2 total-score">Резултат: 0 точки</p>',
        'options': [
        '<img class="a3" src="images/life.png">',
        '<img class="a3" src="images/m.png">',
        '<img class="a3" src="images/z.png">'
        ],
        'correctIndex': 0,
        'points': 10,
        'correctResponse': '<span class="count l5">4</span><h1>Качествено образование</h1><p class="fs30">Около 103 милиона младежи по света нямат основно ниво на грамотност. Пандемията пък е причина към април 2020 г. близо 1,6 милиона деца и младежи да не могат да посещават училище.</p><p class="fs30">Равният достъп до качествено образование е задължително условие за подобряване живота на хората. Целта е до 2030 г. максимално да се намали броят на необразованите хора и да се увеличи броят на хората със специфични познания и умения.</p>',
        'incorrectResponse': '<span class="count l5">4</span><h1>Качествено образование</h1><p class="fs30">Около 103 милиона младежи по света нямат основно ниво на грамотност. Пандемията пък е причина към април 2020 г. близо 1,6 милиона деца и младежи да не могат да посещават училище.</p><p class="fs30">Равният достъп до качествено образование е задължително условие за подобряване живота на хората. Целта е до 2030 г. максимално да се намали броят на необразованите хора и да се увеличи броят на хората със специфични познания и умения.</p>',
      },
      {
        'q': '<span class="ask fs50">Качествено образование</span><img class="p-5 q4-img" src="images/book.png"><p class="py-2 total-score">Резултат: 0 точки</p>',
        'options': [
        '<img class="a4" src="images/pencil.png">',
        '<img class="a4" src="images/paper-plane.png">',
        '<img class="a4" src="images/picture.png">'
        ],
        'correctIndex': 0,
        'points': 10,
        'correctResponse': '<span class="count l5">5</span><h1>Равенство межу половете</h1><p class="fs30">Знаеш ли, че 35% от всички жени по света са били жертва на насилие, а цели 750 милиона са сключили брак, преди да навършат 18 години?</p><p class="fs30">Въпреки че в последните години подобни данни бележат спад, неравенството между половете в политически, икономически и социален аспект е огромно. Елиминирането му е решаващ фактор в изграждането на един мирен, проспериращ и устойчив свят.</p>',
        'incorrectResponse': '<span class="count l5">5</span><h1>Равенство межу половете</h1><p class="fs30">Знаеш ли, че 35% от всички жени по света са били жертва на насилие, а цели 750 милиона са сключили брак, преди да навършат 18 години?</p><p class="fs30">Въпреки че в последните години подобни данни бележат спад, неравенството между половете в политически, икономически и социален аспект е огромно. Елиминирането му е решаващ фактор в изграждането на един мирен, проспериращ и устойчив свят.</p>',
      },
      {
        'q': '<span class="ask fs50">Равенство между половете</span><img class="p-5 q5-img" src="images/female.png"><p class="py-2 total-score">Резултат: 0 точки</p>',
        'options': [
        '<img class="a5" src="images/equal.png">',
        '<img class="a5" src="images/x.png">',
        '<img class="a5" src="images/smile.png">'
        ],
        'correctIndex': 0,
        'points': 10,
        'correctResponse': '<span class="count l5">6</span><h1>Чиста вода и санитарно-хигиенни условия</h1><p class="fs30">Един на всеки трима души в света няма достъп до безопасна питейна вода, а 2,3 милиарда от световното население не разполага с основни санитарно-хигиенни условия.</p><p class="fs30">Очаква се до 2050 г. един на четирима души да страда от недостиг на вода – статистика, която можем да променим само с инвестиране в адекватна инфраструктура и опазване на водната екосистема. За да има безопасна и достъпна питейна вода за всички.</p>',
        'incorrectResponse': '<span class="count l5">6</span><h1>Чиста вода и санитарно-хигиенни условия</h1><p class="fs30">Един на всеки трима души в света няма достъп до безопасна питейна вода, а 2,3 милиарда от световното население не разполага с основни санитарно-хигиенни условия.</p><p class="fs30">Очаква се до 2050 г. един на четирима души да страда от недостиг на вода – статистика, която можем да променим само с инвестиране в адекватна инфраструктура и опазване на водната екосистема. За да има безопасна и достъпна питейна вода за всички.</p>',
      },
      {
        'q': '<span class="ask fs50">Чиста вода и санитарно-хигиенни условия</span><img class="p-5 mt-5 q6-img" src="images/water.png"><p class="py-2 total-score">Резултат: 0 точки</p>',
        'options': [
        '<img class="a6" src="images/juice.png">',
        '<img class="a6" src="images/drop.png">',
        '<img class="a6" src="images/trash.png">'
        ],
        'correctIndex': 1,
        'points': 10,
        'correctResponse': '<span class="count l5">7</span><h1>Възобновяема енергия</h1><p class="fs30">Енергийното потребление е причината за 60% от парниковите газове, причиняващи климатичните промени, а 3 милиарда души по света още използват вредни горива при готвене.</p><p class="fs30">Разширяването на инфраструктурата, подобряването на енергийната ефективност и инвестирането в иновативни технологии за чиста, ефективна и възобновяема енергия навсякъде ще помогне както на човечеството, така и на околната среда.</p>',
        'incorrectResponse': '<span class="count l5">7</span><h1>Възобновяема енергия</h1><p class="fs30">Енергийното потребление е причината за 60% от парниковите газове, причиняващи климатичните промени, а 3 милиарда души по света още използват вредни горива при готвене.</p><p class="fs30">Разширяването на инфраструктурата, подобряването на енергийната ефективност и инвестирането в иновативни технологии за чиста, ефективна и възобновяема енергия навсякъде ще помогне както на човечеството, така и на околната среда.</p>',
      },
      {
        'q': '<span class="ask fs50">Възобновяема енергия</span><img class="p-5 q7-img" src="images/stop.png"><p class="py-2 total-score">Резултат: 0 точки</p>',
        'options': [
        '<img class="a7" src="images/xx.png">',
        '<img class="a7" src="images/guitar.png">',
        '<img class="a7" src="images/energy.png">'
        ],
        'correctIndex': 2,
        'points': 10,
        'correctResponse': '<span class="count l5">8</span><h1>Сигурна работа и икономически растеж</h1><p class="fs30">През 2019 г. световната безработица е била 5,4%. Очаква се заради кризата, породена от пандемията, 174 милиона души в цял свят да останат без работа през 2020 г.</p><p class="fs30">Насърчаването на иновациите, предприемачеството и създаването на работни места е ключово за икономическия растеж. То може да премахне принудителния труд и трафика на хора. Целта е до 2030 г. да осигурим пълна заетост и достоен труд за всички.</p>',
        'incorrectResponse': '<span class="count l5">8</span><h1>Сигурна работа и икономически растеж</h1><p class="fs30">През 2019 г. световната безработица е била 5,4%. Очаква се заради кризата, породена от пандемията, 174 милиона души в цял свят да останат без работа през 2020 г.</p><p class="fs30">Насърчаването на иновациите, предприемачеството и създаването на работни места е ключово за икономическия растеж. То може да премахне принудителния труд и трафика на хора. Целта е до 2030 г. да осигурим пълна заетост и достоен труд за всички.</p>',
      },
      {
        'q': '<span class="ask fs50">Сигурна работа и икономически растеж</span><img class="p-5 q8-img" src="images/grow.png"><p class="py-2 total-score">Резултат: 0 точки</p>',
        'options': [
        '<img class="a8" src="images/up.png">',
        '<img class="a8" src="images/down.png">',
        '<img class="a8" src="images/upup.png">'
        ],
        'correctIndex': 0,
        'points': 10,
        'correctResponse': '<span class="count l5">9</span><h1>Иновации и инфраструктура</h1><p class="fs30">Днес 46% от хората в света нямат достъп до интернет. По време на пандемията това ги оставя и без възможност за онлайн обучение, работа и дистанционна здравна консултация.</p><p class="fs30">За да посрещнем бъдещите предизвикателства, световните индустрия и инфраструктура трябва да бъдат модернизирани. За целта е важно да насърчаваме иновативните устойчиви технологии и да гарантираме равен и универсален достъп до информация за всеки.</p>',
        'incorrectResponse': '<span class="count l5">9</span><h1>Иновации и инфраструктура</h1><p class="fs30">Днес 46% от хората в света нямат достъп до интернет. По време на пандемията това ги оставя и без възможност за онлайн обучение, работа и дистанционна здравна консултация.</p><p class="fs30">За да посрещнем бъдещите предизвикателства, световните индустрия и инфраструктура трябва да бъдат модернизирани. За целта е важно да насърчаваме иновативните устойчиви технологии и да гарантираме равен и универсален достъп до информация за всеки.</p>',
      },
      {
        'q': '<span class="ask fs50">Иновации и инфраструктура</span><img class="p-5 q9-img" src="images/cube-main.png"><p class="py-2 total-score">Резултат: 0 точки</p>',
        'options': [
        '<img  class="a9" src="images/cube-down.png">',
        '<img  class="a9" src="images/cube-fill.png">',
        '<img  class="a9" src="images/cube-up.png">'
        ],
        'correctIndex': 2,
        'points': 10,
        'correctResponse': '<span class="count">10</span><h1>Намаляване на неравенствата</h1><p class="fs30">Един на всеки петима души в света става жертва на някакъв вид дискриминация – при хората с увреждания това са трима от всеки десет души.</p><p class="fs30">Неравенството – икономическо, финансово, социално, полово - продължава да съществува и дори се задълбочава в резултат на настъпилата криза. Общата цел е да го редуцираме толкова, че всеки да има равен шанс за успех независимо от пола, расата, възрастта или религията си.</p>',
        'incorrectResponse': '<span class="count">10</span><h1>Намаляване на неравенствата</h1><p class="fs30">Един на всеки петима души в света става жертва на някакъв вид дискриминация – при хората с увреждания това са трима от всеки десет души.</p><p class="fs30">Неравенството – икономическо, финансово, социално, полово - продължава да съществува и дори се задълбочава в резултат на настъпилата криза. Общата цел е да го редуцираме толкова, че всеки да има равен шанс за успех независимо от пола, расата, възрастта или религията си.</p>',
      },
      {
        'q': '<span class="ask fs50">Намаляване на неравенствата</span><img class="p-5 q10-img" src="images/equality.png"><p class="py-2 total-score">Резултат: 0 точки</p>',
        'options': [
        '<img class="a10" src="images/dots.png">',
        '<img class="a10" src="images/arrow.png">',
        '<img class="a10" src="images/four.png">'
        ],
        'correctIndex': 1,
        'points': 10,
        'correctResponse': '<span class="count">11</span><h1>Устойчиви градове и общности</h1><p class="fs30">Цели 60% от хората по света живеят в градове. Градовете от своя страна генерират 75% от въглеродните емисии, замърсяващи атмосферата на Земята. </p><p class="fs30">Урбанизацията се случва все по-бързо и е важно да направим градовете устойчиви чрез подобряване на тяхното планиране и управление. Това означава да създадем възможности за кариера, бизнес и достъпни жилища, обществен транспорт и повече зелени пространства.</p>',
        'incorrectResponse': '<span class="count">11</span><h1>Устойчиви градове и общности</h1><p class="fs30">Цели 60% от хората по света живеят в градове. Градовете от своя страна генерират 75% от въглеродните емисии, замърсяващи атмосферата на Земята. </p><p class="fs30">Урбанизацията се случва все по-бързо и е важно да направим градовете устойчиви чрез подобряване на тяхното планиране и управление. Това означава да създадем възможности за кариера, бизнес и достъпни жилища, обществен транспорт и повече зелени пространства.</p>',
      },
      {
        'q': '<span class="ask fs50">Устойчиви градове и общности</span><img class="p-5 q11-img" src="images/building.png"><p class="py-2 total-score">Резултат: 0 точки</p>',
        'options': [
        '<img class="a11" src="images/bulldozer.png">',
        '<img class="a11" src="images/house.png">',
        '<img class="a11" src="images/flower.png">'
        ],
        'correctIndex': 1,
        'points': 10,
        'correctResponse': '<span class="count">12</span><h1>Отговорно потребление</h1><p class="fs30">Населението на Земята може да достигне 9,5 милиарда души през 2050 г. За да оцелее човечеството тогава, ще се нуждае от количество природни ресурси, равно на цели три планети.</p><p class="fs30">В полза на нашето оцеляване е да работим за устойчиви производство и потребление. Така можем да постигнем икономически прогрес, като същевременно намалим вредата за околната среда и повишим ефективното ползване на ресурсите.</p>',
        'incorrectResponse': '<span class="count">12</span><h1>Отговорно потребление</h1><p class="fs30">Населението на Земята може да достигне 9,5 милиарда души през 2050 г. За да оцелее човечеството тогава, ще се нуждае от количество природни ресурси, равно на цели три планети.</p><p class="fs30">В полза на нашето оцеляване е да работим за устойчиви производство и потребление. Така можем да постигнем икономически прогрес, като същевременно намалим вредата за околната среда и повишим ефективното ползване на ресурсите.</p>',
      },
      {
        'q': '<span class="ask fs50">Отговорно потребление</span><img class="p-5 q12-img" src="images/infinity.png"><p class="py-2 total-score">Резултат: 0 точки</p>',
        'options': [
        '<img class="a12" src="images/rectangle.png">',
        '<img class="a12" src="images/play.png">',
        '<img class="a12" src="images/pouse.png">'
        ],
        'correctIndex': 1,
        'points': 10,
        'correctResponse': '<span class="count">13</span><h1>Борба с климатичните проблеми</h1><p class="fs30">Глобалните въглеродни емисии трябва да паднат с 50% до 2030-а и да се занулят до 2050-а година, ако искаме да задържим увеличението на световната температура под 2°C.</p><p class="fs30">Изминалото десетилетие бе най-горещото в историята и причини природни бедствия, засягащи милиони хора. Катастрофалният изход е неизбежен, ако не действаме сега, за да оформим устойчива световна икономика – полезна за хората и планетата.</p>',
        'incorrectResponse': '<span class="count">13</span><h1>Борба с климатичните проблеми</h1><p class="fs30">Глобалните въглеродни емисии трябва да паднат с 50% до 2030-а и да се занулят до 2050-а година, ако искаме да задържим увеличението на световната температура под 2°C.</p><p class="fs30">Изминалото десетилетие бе най-горещото в историята и причини природни бедствия, засягащи милиони хора. Катастрофалният изход е неизбежен, ако не действаме сега, за да оформим устойчива световна икономика – полезна за хората и планетата.</p>',
      },
      {
        'q': '<span class="ask fs50">Борба с климатичните проблеми</span><img class="p-5 q13-img" src="images/eye.png"><p class="py-2 total-score">Резултат: 0 точки</p>',
        'options': [
        '<img class="a13" src="images/iris.svg">',
        '<img class="a13" src="images/naruto.png">',
        '<img class="a13" src="images/earth.svg">'
        ],
        'correctIndex': 2,
        'points': 10,
        'correctResponse': '<span class="count">14</span><h1>Живот под водата</h1><p class="fs30">Световният океан абсорбира 30% от въглеродния диоксид, произведен човешката дейност. Тези емисии са причината за затопляне на водата, окислението и загубата на кислород.</p><p class="fs30">Водното биоразнообразие е от решаващо значение за здравето на хората и нашата планета. Затова е важно внимателно да управляваме този ценен ресурс, като се стремим към ограничаване на свръхриболова и замърсяването.</p>',
        'incorrectResponse': '<span class="count">14</span><h1>Живот под водата</h1><p class="fs30">Световният океан абсорбира 30% от въглеродния диоксид, произведен човешката дейност. Тези емисии са причината за затопляне на водата, окислението и загубата на кислород.</p><p class="fs30">Водното биоразнообразие е от решаващо значение за здравето на хората и нашата планета. Затова е важно внимателно да управляваме този ценен ресурс, като се стремим към ограничаване на свръхриболова и замърсяването.</p>',
      },
      {
        'q': '<span class="ask fs50">Живот под водата</span><img class="p-5 q14-img" src="images/wave.png"><p class="py-2 total-score">Резултат: 0 точки</p>',
        'options': [
        '<img class="a14" src="images/fish.png">',
        '<img class="a14" src="images/bones.png">',
        '<img class="a14" src="images/eating.png">'
        ],
        'correctIndex': 0,
        'points': 10,
        'correctResponse': '<span class="count">15</span><h1>Живот на земята</h1><p class="fs30">Над 2 милиарда хектара гори в световен мащаб са унищожени в резултат на човешка дейност, понижавайки качеството на живот на близо 3,2 милиарда души.</p><p class="fs30">Разрушаването на екосистемите е причина и за рязък скок в заболяванията, предавани от животни на хора и обратно. Ето защо възстановяването на природата е от решаващо значение за подобряването на живота и намаляването на рисковете за икономиката в дългосрочен план.</p>',
        'incorrectResponse': '<span class="count">15</span><h1>Живот на земята</h1><p class="fs30">Над 2 милиарда хектара гори в световен мащаб са унищожени в резултат на човешка дейност, понижавайки качеството на живот на близо 3,2 милиарда души.</p><p class="fs30">Разрушаването на екосистемите е причина и за рязък скок в заболяванията, предавани от животни на хора и обратно. Ето защо възстановяването на природата е от решаващо значение за подобряването на живота и намаляването на рисковете за икономиката в дългосрочен план.</p>',
      },
       {
        'q': '<span class="ask fs50">Живот под водата</span><img class="p-5 q15-img" src="images/birds.png"><p class="py-2 total-score">Резултат: 0 точки</p>',
        'options': [
        '<img class="a15" src="images/cloud.png">',
        '<img class="a15" src="images/tree.png">',
        '<img class="a15" src="images/factory.png">'
        ],
        'correctIndex': 1,
        'points': 10,
        'correctResponse': '<span class="count">16</span><h1>Мир и справедливост</h1><p class="fs30">През 2019 г. са регистрирани общо 357 убити и 30 изчезнали правозащитници, журналисти и синдикалисти по данни от 47 държави.</p><p class="fs30">Програмата на ООН цели да намали значително всички форми на насилие, като си партнира с правителствата и общностите. Насърчаването на върховенството на закона и правата на човека са от ключово значение за този процес.</p>',
        'incorrectResponse': '<span class="count">16</span><h1>Мир и справедливост</h1><p class="fs30">През 2019 г. са регистрирани общо 357 убити и 30 изчезнали правозащитници, журналисти и синдикалисти по данни от 47 държави.</p><p class="fs30">Програмата на ООН цели да намали значително всички форми на насилие, като си партнира с правителствата и общностите. Насърчаването на върховенството на закона и правата на човека са от ключово значение за този процес.</p>',
      },
      {
        'q': '<span class="ask fs50">Мир и справедливост</span><img class="p-5 q16-img" src="images/hammer.png"><p class="py-2 total-score">Резултат: 0 точки</p>',
        'options': [
        '<img class="a16" src="images/like.png">',
        '<img class="a16" src="images/handcuffs.png">',
        '<img class="a16" src="images/swallow.png">'
        ],
        'correctIndex': 2,
        'points': 10,
        'correctResponse': '<span class="count">17</span><h1>Партньорства за целите</h1><p class="fs30">Прогнозите сочат, че в следствие на пандемията глобалната икономика ще се свие рязко - с 3%, през 2020 г. Това би довело до най-лошата рецесия след Голямата депресия.</p><p class="fs30">Преодоляването на настоящата и всяка друга световна криза би било възможно единствено ако всички страни работят заедно – обединени от стабилни принципи, еднакви ценности и споделена глобална визия.</p>',
        'incorrectResponse': '<span class="count">17</span><h1>Партньорства за целите</h1><p class="fs30">Прогнозите сочат, че в следствие на пандемията глобалната икономика ще се свие рязко - с 3%, през 2020 г. Това би довело до най-лошата рецесия след Голямата депресия.</p><p class="fs30">Преодоляването на настоящата и всяка друга световна криза би било възможно единствено ако всички страни работят заедно – обединени от стабилни принципи, еднакви ценности и споделена глобална визия.</p>',
      },
      {
        'q': '<span class="ask fs50">Партньорства за целите</span><img class="p-5 q17-img" src="images/flower1.png"><p class="py-2 total-score">Резултат: 0 точки</p>',
        'options': [
        '<img class="a17" src="images/liga.png">',
        '<img class="a17" src="images/flower2.png">',
        '<img class="a17" src="images/flower3.png">'
        ],
        'correctIndex': 1,
        'points': 10,
        'correctResponse': '<div class="theEnd"><span class="py-2 score fs50">Твоят резултат е: 0 точки</span></div>',
        'incorrectResponse': '<div class="theEnd"><span class="py-2 score fs50">Твоят резултат е: 0 точки</span></div>',
      },
    ]
  });

</script>

<style>
@font-face {
   font-family: 'bnpp-sans';
   src: url("font/bnpp-sans.ttf") format("truetype");
}
@font-face {
   font-family: 'bnpp-sans-bold';
   src: url("font/bnpp-sans-bold.ttf") format("truetype");
}
@font-face {
   font-family: 'bnpp-sans-cond-bold-v2';
   src: url("font/bnpp-sans-cond-bold-v2.ttf") format("truetype");
}
@font-face {
   font-family: 'bnpp-sans-cond-extrabold-v2';
   src: url("font/bnpp-sans-cond-extrabold-v2.ttf") format("truetype");
}
@font-face {
   font-family: 'bnpp-sans-cond-light-v2';
   src: url("font/bnpp-sans-cond-light-v2.ttf") format("truetype");
}
@font-face {
   font-family: 'bnpp-sans-cond-v2';
   src: url("font/bnpp-sans-cond-v2.ttf") format("truetype");
}
@font-face {
   font-family: 'bnpp-sans-extrabold';
   src: url("font/bnpp-sans-extrabold.ttf") format("truetype");
}
@font-face {
   font-family: 'bnpp-sans-light';
   src: url("font/bnpp-sans-light.ttf") format("truetype");
}
body {
    margin: 0;
}
</style>
</body>
</html>