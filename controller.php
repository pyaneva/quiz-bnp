<?php

require_once('connect.php');

if(!empty($_POST["name"]) && !empty($_POST["profession"]) && !empty($_POST["supportCause"])) {
    
    $name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
    $profession = filter_var($_POST['profession'], FILTER_SANITIZE_STRING);
    $support_cause = filter_var($_POST['supportCause'], FILTER_SANITIZE_STRING);
    $idea = filter_var($_POST['idea'], FILTER_SANITIZE_STRING);
    $score = filter_var($_POST['score'], FILTER_SANITIZE_STRING);

    $name = mysqli_real_escape_string($conn, $name);
    $profession = mysqli_real_escape_string($conn, $profession);
    $support_cause = mysqli_real_escape_string($conn, $support_cause);
    $idea = mysqli_real_escape_string($conn, $idea);
    $score = mysqli_real_escape_string($conn, $score);

    $sql = "INSERT INTO results (name, profession, support_cause, idea, score)
    VALUES ('$name', '$profession', '$support_cause', '$idea', $score)";
    
    if (mysqli_query($conn, $sql)) {
        header("Location:index.php?success=1");

        mysqli_close($conn);
    } else {
      echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }
} 
else {
    echo "Всички полета са задължителни";
}
